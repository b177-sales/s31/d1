const express = require("express");
// Create a Router instance that functions as a routing system
const router = express.Router();
// Import the taskControllers
const taskController = require("../controllers/taskControllers");

// Route to get all the tasks
// endpoint: localhost:3001/tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
// Endpoint: localhost: 3001/tasks/123456
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// 1
// Route to get a task
router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// 5
// Route to change status of a task to complete
router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Export the router object to be used in index.js
module.exports = router;