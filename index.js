// Setup the dependecies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.ueghm.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
		useNewUrlParser : true,
		useUnifiedTopology : true
});

app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));